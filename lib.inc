section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60           ; use exit system call to shut down correctly
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
      cmp byte [rdi+rax], 0
      je .end 
      inc rax
      jmp .loop 
    .end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    cmp rdi,`\0`
    je .print_zero
    call string_length
    mov rsi,rdi
    mov rdx, rax 
    mov rax, 1
    mov rdi, 1 
    syscall
    ret
    .print_zero:
      mov rdi,`0`
      push rdi
      mov rsi,rsp
      mov rdx,1
      mov rax,1
      mov rdi,1
      syscall
      pop rdi
      ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax,1
    mov rsi,rsp
    mov rdx,1
    mov rdi,1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor r9,r9
    xor r10,r10
    mov r8,10
    mov rax , rdi
    .loop:
      xor rdx,rdx
      div r8
      add rdx,`0`
      push rdx
      inc r9
      cmp rax,`\0`
      je .print
      jmp .loop
      .print:
        pop rdi
        call print_char
        inc r10
        cmp r10,r9
        jne .print
    .end:    
      ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jge print_uint 
    neg rdi
    push rdi
    mov rdi,`-`
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    call string_length
    mov r8,rax
    mov rdi,rsi
    call string_length
    cmp r8,rax
    jne .notEquals
    .loop:
      cmp byte [rsi],0
      je .equals
      mov r8b ,byte [rsi]
      cmp byte [rdi],r8b
      jne .notEquals
      inc rsi
      inc rdi
      jmp .loop
    .notEquals:
      mov rax,0
      ret
    .equals:
      mov rax,1
      ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax,0
    mov rsi,rsp
    mov rdx,1
    mov rdi,0
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
mov r8, rdi	
    mov r9, rsi
    mov r10, rdi
    mov r11, 0	
    .loop:
	    cmp r11, r9
	    jae .notEnough
	    push r11
	    call read_char
	    pop r11
	    cmp al, `\0`
	    je .end
	    cmp al, `\t`
	    je .check
	    cmp al, `\n`
	    je .check
	    cmp al, ` `
	    je .check
        mov byte[r8], al    
        inc r8
        inc r11         
        jmp .loop
    .notEnough:
        mov rax, 0
        ret
    .check:
        cmp r11, 0
        je .loop
    .end:
        mov byte[r8], 0
        mov rax, r10
        mov rdx, r11
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    call string_length      
    mov rsi, rax             
    xor rcx, rcx              
    xor rdx, rdx            
    xor rax, rax    
    xor r8, r8                  
.loop:
    mov r8b, byte [rdi + rcx]  
    cmp r8b, `\0`              
    je .end                  
    cmp r8b, `9`               
    ja .end                    
    cmp r8b, `0`                 
    jb .end                 
    sub r8b, `0`                 
    imul rax, 10                
    add rax, r8                 
    inc rdx                     
    inc rcx                     
    jmp .loop               
.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r8,r8
    mov r8b,[rdi]
    cmp r8b,'-'
    je .neg
    jmp parse_uint
    .neg:
      inc rdi
      call parse_uint
      neg rax
      inc rdx
      ret
    .pos:
      inc rdi
      call parse_uint
      inc rdx
      ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    call string_length
    cmp rax,rdx
    jg .print_zero
    .loop:
    mov rdx, [rdi]
    mov [rsi], rdx
    cmp dl,0 
    je .end
    inc rdi
    inc rsi
    jmp .loop
    .end:
      ret
    .print_zero:
      mov rax,0
      ret

